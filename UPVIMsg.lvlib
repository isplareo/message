﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Creation" Type="Folder">
		<Item Name="Msg Create Message Queue.vi" Type="VI" URL="../Msg Create Message Queue.vi"/>
		<Item Name="Msg Create UI Message Queue.vi" Type="VI" URL="../Msg Create UI Message Queue.vi"/>
		<Item Name="Msg Create UI PSP Listener.vi" Type="VI" URL="/&lt;userlib&gt;/Message/Msg Create UI PSP Listener.vi"/>
	</Item>
	<Item Name="Destruction" Type="Folder">
		<Item Name="Msg Destroy Message Queue.vi" Type="VI" URL="/&lt;userlib&gt;/Message/Msg Destroy Message Queue.vi"/>
	</Item>
	<Item Name="Get" Type="Folder">
		<Item Name="Msg Get Message Periodic.vi" Type="VI" URL="../Msg Get Message Periodic.vi"/>
		<Item Name="Msg Get Message.vi" Type="VI" URL="../Msg Get Message.vi"/>
	</Item>
	<Item Name="Send/Post" Type="Folder">
		<Item Name="Msg Post Error.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Post Error.vi"/>
		<Item Name="Msg Post Message.vi" Type="VI" URL="../Msg Post Message.vi"/>
		<Item Name="Msg Priority Message.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Priority Message.vi"/>
		<Item Name="Msg Send Message.vi" Type="VI" URL="../Msg Send Message.vi"/>
		<Item Name="Msg Send Priority Message.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Send Priority Message.vi"/>
		<Item Name="Msg Send Response.vi" Type="VI" URL="../Msg Send Response.vi"/>
	</Item>
	<Item Name="subVIs" Type="Folder">
		<Item Name="Message Cluster.ctl" Type="VI" URL="../Message Cluster.ctl"/>
		<Item Name="MetronomeLoop.vi" Type="VI" URL="/&lt;userlib&gt;/Message/MetronomeLoop.vi"/>
		<Item Name="MetronomeMessage.ctl" Type="VI" URL="/&lt;userlib&gt;/Message/MetronomeMessage.ctl"/>
		<Item Name="Msg Post Queue Error.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Post Queue Error.vi"/>
		<Item Name="Msg Post Queue Message.vi" Type="VI" URL="../Msg Post Queue Message.vi"/>
		<Item Name="Msg Post UI Error.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Post UI Error.vi"/>
		<Item Name="Msg Post UI Message.vi" Type="VI" URL="../Msg Post UI Message.vi"/>
		<Item Name="Msg Priority Queue Message.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Priority Queue Message.vi"/>
		<Item Name="Msg Priority UI Message.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Priority UI Message.vi"/>
		<Item Name="Msg Send Priority Queue Message.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Send Priority Queue Message.vi"/>
		<Item Name="Msg Send Priority UI Message.vi" Type="VI" URL="/Orbion/FlowUniformity/subVIs/Msg/Msg Send Priority UI Message.vi"/>
		<Item Name="Msg Send Queue Message.vi" Type="VI" URL="../Msg Send Queue Message.vi"/>
		<Item Name="Msg Send UI Message.vi" Type="VI" URL="../Msg Send UI Message.vi"/>
		<Item Name="StartMetronome.vi" Type="VI" URL="/&lt;userlib&gt;/Message/StartMetronome.vi"/>
		<Item Name="StopMetronome.vi" Type="VI" URL="/&lt;userlib&gt;/Message/StopMetronome.vi"/>
		<Item Name="Test Metronome.vi" Type="VI" URL="/&lt;userlib&gt;/Message/Test Metronome.vi"/>
	</Item>
</Library>
